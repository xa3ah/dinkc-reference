var voyeur;             // peek to function definition
var voyeurHidden;
var panel;
var mainHeaderHeight;

var searchfield;
var searchResult;
var allfuncs = [];
var mostSearched = [];

function initialize() {
    loadElements();     // get elements
    renderTables();     // renders all tables and attach event listeners, if any
    loadEvents();       // attach event listener on 't' (tag) class. They provide function definition peek.
    formatCode();       // trims extra whitespace from <pre> tags
}

function loadEvents() {
    let anchors = document.getElementsByTagName('a');
    for (let a of anchors) {
        const text = a.innerText;
        let t = text.substr(text.length - 2);

        if (t === '()') {
            a.classList.add('tag');
        }
    }

    let elements = document.getElementsByClassName('tag');
    for (let el of elements) {
        const fnName = el.innerText.substring(0, el.innerText.length - 2);
        const targetId = `${fnName}-def`;
        
        const hasFunc = allfuncs.some(x => x.name === el.innerHTML);
        if(!hasFunc) {
            allfuncs.push({ name: el.innerHTML, link: `#${targetId}`});
        }

        el.addEventListener('click', () => setAnchor(targetId));
        el.addEventListener('mouseover', () => showVoyeur(targetId));
        el.addEventListener('mouseout', () => hideVoyeur());
    }
}

function loadElements() {
    voyeur = document.getElementById('voyeur');
    voyeurHidden = document.getElementById('voyeur-hidden');
    panel = document.getElementsByClassName('right-panel')[0];
    
    searchfield = document.getElementById('searchfield');
    searchResult = document.getElementById('search-result');

    mainHeaderHeight =
        document.getElementsByClassName('main-header')[0].offsetHeight;
}

function formatCode() {
    let elements = document.getElementsByTagName('pre');
    for (let el of elements) {
        let text = el.innerText;
        let lines = text.match(/[^\r\n]+/g);
        el.innerHTML = '';

        lines.forEach((line, i) => lines[i] = line.trim());
        lines = indentLines(lines);
        lines.forEach(l => el.innerHTML += l);
    }
}

function indentLines(lines) {
    let indentSize = ['&nbsp;', '&nbsp;', '&nbsp;', '&nbsp;'];
    let indentation = '';

    lines.forEach((line, i) => {
        if (line[0] === '{') {
            lines[i] = indentation + line;
            indentSize.forEach(i => indentation += i);
        }
        else if (line[0] === '}') {
            indentSize.forEach(i => indentation = indentation.replace(i, ''));
            lines[i] = indentation + line;
        }
        else {
            lines[i] = indentation + line;
        }
        lines[i] += '\n';
        lines[i] = fixVarName(lines[i]);
    });

    return lines;
}

function fixVarName(variable) {
    return variable.replace(/¤t_sprite/, '&amp;current_sprite');
}

function setAnchor(targetId) {
    let url = location.href;
    location.href = '#' + targetId;
}

function showVoyeur(targetId) {
    const el = document.getElementById(targetId);
    voyeurHidden.innerHTML = el.innerHTML;

    voyeur.style.cssText = setVoyeurStyle(1, 999);
    voyeur.innerHTML = el.innerHTML;

}

function hideVoyeur() {
    const style = setVoyeurStyle(0, -999);
    voyeur.style.cssText = style;
}

function setVoyeurStyle(opacity, zIndex) {
    const e = window.event;
    const x = getOffsetX(e.pageX);
    const y = getOffsetY(e.pageY);
    const style =
        `position: absolute; top: ${y + 10}px; left: ${x}px; opacity: ${opacity}; z-index: ${zIndex}; min-width: ${voyeurHidden.offsetWidth}px;`;

    return style;
}

function getOffsetX(mouseX) {
    const panelXOffset = screen.width - panel.offsetWidth;
    const mouseXOffset = mouseX - panelXOffset;
    const voyeurlWidth = mouseXOffset + voyeurHidden.offsetWidth;

    let xOffset = 0;
    if (voyeurlWidth < panel.offsetWidth) {
        xOffset = mouseXOffset;
    }

    return xOffset;
}

function getOffsetY(mouseY) {
    const yOffset = mouseY - mainHeaderHeight + 70;

    return yOffset;
}


function setFocus() {
    searchfield.focus();
    panel.style.opacity = '0.5';
}

function resetFocus() {
    panel.style.opacity = '1';
}

function search() {
    if (searchfield.value === '') {
        searchResult.innerHTML = '';
        return;
    }

    searchResult.innerHTML = '';
    let result = allfuncs.filter(x => x.name.includes(searchfield.value));
    result.forEach(x => {
        const fnName = x.name.substring(0, x.name.length - 2);
        const anchor = `#${fnName}-def`;
        searchResult.innerHTML += 
            `<a onclick="resetFocus()" class="w3-bar-item w3-button" href="${anchor}">${x.name}</a>`;
    });
}
