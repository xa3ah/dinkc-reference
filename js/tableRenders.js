function generateRows(commands) {
    const length = commands.length;
    let rows = '';

    for (let i = 0; i < length; i++) {
        const anchor = commands[i].name.substring(0, commands[i].name.length);
        const row = 
            `<tr>
                <td><a class="tag" href="#${anchor}-def">${commands[i].name}</a></td>
                <td>${commands[i].description}</td>
            </tr>`;
        
        rows += row;
    }
    
    return rows;
}

function renderTables() {
    const commands0 = [
        { name: 'preload_seq()', description: `load graphics into cache`},
        { name: 'sp_base_attack()', description: `base value from which to derive attack sequences`},
        { name: 'sp_base_death()', description: `base value from which to derive sprite's death graphic`},
        { name: 'sp_base_idle()', description: `base value from which to derive standing-still seq's`},
        { name: 'sp_base_walk()', description: `base value from which to derive walking sequences` },
        { name: 'sp_brain()', description: `basic control of animation sequences` },
        { name: 'sp_frame()', description: `change the next frame to be shown in an animation` },
        { name: 'sp_frame_delay()', description: `set/detect animation speed` },
        { name: 'sp_nocontrol()', description: `freeze sprite until an animation is played` },
        { name: 'sp_pframe()', description: `set/detect frame part of sprite's graphic` },
        { name: 'sp_picfreeze()', description: `unknown` },
        { name: 'sp_pseq()', description: `set/detect sequence part of sprite's graphic` },
        { name: 'sp_reverse()', description: `cause animation to run in reverse` },
        { name: 'sp_seq()', description: `set/detect animation sequence` }

    ];
    const commands1 = [
        { name: 'copy_bmp_to_screen()', description: `replace screen background with bitmap`},
        { name: 'create_sprite()', description: `dynamically create a sprite`},
        { name: 'dink_can_walk_off_screen()', description: `scene can continue after Dink leaves`},
        { name: 'draw_hard_map()', description: `recalculate hardness after modifying sprites`},
        { name: 'draw_hard_sprite()', description: `recalculate hardness after modifying one sprite`},
        { name: 'draw_screen()', description: `draw last screen loaded`},
        { name: 'draw_status()', description: `draw or re-draw status area and sidebars`},
        { name: 'fade_down()', description: `fade-to-black effect`},
        { name: 'fade_up()', description: `undo fade_down()`},
        { name: 'fill_screen()', description: `fill background & status areas with a single color`},
        { name: 'force_vision()', description: `change vision during a cut scene`},
        { name: 'freeze()', description: `stop Dink or another sprite from moving`},
        { name: 'load_screen()', description: `prepare to draw a screen but don't draw it`},
        { name: 'move()', description: `move sprite to a new location on the screen`},
        { name: 'move_stop()', description: `move sprite while pausing script`},
        { name: 'unfreeze()', description: `cancel the effect of freeze()`},
        { name: 'wait()', description: `pause this script, allow others to run`}
    ];

    const commands2 = [
        { name: 'add_exp()', description: `add to &exp "visually correct"` },
        { name: 'dink_can_walk_off_screen()', description: `scene can continue after Dink leaves` },
        { name: 'push_active()', description: `is Dink allowed to push?` },
        { name: 'set_dink_speed()', description: `set speed Dink can move` },
        { name: 'sp_brain()', description: `brains 1 and 13 are for sprite 1 only` }
    ];

    const commands3 = [
        { name: 'editor_frame()', description: `specify replacement frame for sprite's graphic` },
        { name: 'editor_seq()', description: `specify replacement seq. for sprite's graphic` },
        { name: 'editor_type()', description: `specify how sprite is to be loaded from map.dat` },
        { name: 'sp()', description: `get DinkEdit sprite # for dink engine sprite #` },
        { name: 'sp_editor_num()', description: `get dink engine sprite # for DinkEdit sprite #` }
    ];

    const commands4 = [
        { name: 'screenlock()', description: `prevent Dink from leaving the screen` },
        { name: 'set_callback_random()', description: `repeatedly call another procedure in this script` },
        { name: 'sp_attack_wait()', description: `delay a monster's attacking for this long` },
        { name: 'sp_base_attack()', description: `set attack sequences so enemy will attack` },
        { name: 'sp_base_death()', description: `base value for sprite's death graphic` },
        { name: 'sp_brain()', description: `select enemy's basic motion` },
        { name: 'sp_distance()', description: `how close Dink must get before monster attacks` },
        { name: 'sp_exp()', description: `set experience awarded to Dink if he kill sprite` },
        { name: 'sp_range()', description: `specify how far an enemy's attack reaches` },
        { name: 'sp_target()', description: `specify monster's attack target` },
        { name: 'sp_touch_damage()', description: `set action caused by Dink touching sprite` }
    ];

    const commands5 = [
        { name: 'add_item()', description: `add an item to the weapons area of Dink's pack` },
        { name: 'add_magic()', description: `add an item to the magic area of Dink's pack` },
        { name: 'arm_magic()', description: `arm the spell in pack slot &cur_magic` },
        { name: 'arm_weapon()', description: `arm the weapon in pack slot &cur_weapon` },
        { name: 'compare_weapon()', description: `identify armed weapon by script name` },
        { name: 'count_item()', description: `count specific weapon items in the pack` },
        { name: 'count_magic()', description: `count specific magic items in the pack` },
        { name: 'free_items()', description: `count remaining free item slots in pack` },
        { name: 'free_magic()', description: `count remaining free magic slots in pack` },
        { name: 'kill_cur_item()', description: `delete currently armed weapon item` },
        { name: 'kill_cur_magic()', description: `delete currently armed magic item` },
        { name: 'kill_this_item()', description: `delete named weapon item from pack` },
        { name: 'kill_this_magic()', description: `delete named magic item from pack` }
    ];

    const commands6 = [
        { name: 'set_keep_mouse()', description: `keep mouse active during gameplay` },
        { name: 'set_mode()', description: `set mouse or normal game-play mode` },
        { name: 'sp_brain()', description: `brain 13 is mouse brain` }   
    ];

    const commands7 = [
        { name: 'choice()', description: `display menu and/or information to player` },
        { name: 'stop_entire_game()', description: `pause game while choice() menu is displayed` },
        { name: 'stop_wait_for_button()', description: `cancel another script's wait_for_button()` },
        { name: 'wait_for_button()', description: `set &result based on key pressed by user` }
    ];

    const commands8 = [
        { name: 'copy_bmp_to_screen()', description: `replace screen background with bitmap` },
        { name: 'draw_background()', description: `refresh screen background without running scripts` },
        { name: 'draw_hard_map()', description: `recalculate hardness after modifying sprites` },
        { name: 'draw_hard_sprite()', description: `recalculate hardness after modifying one sprite` },
        { name: 'draw_screen()', description: `draw last screen loaded` },
        { name: 'draw_status()', description: `draw or re-draw status area and sidebars` },
        { name: 'fade_down()', description: `fade-to-black effect` },
        { name: 'fade_up()', description: `undo fade_down()` },
        { name: 'fill_screen()', description: `fill background & status areas with a single color` },
        { name: 'load_screen()', description: `prepare to draw a screen but don't draw it` },
        { name: 'show_bmp()', description: `display map or other full-screen bitmap` },
        { name: 'sp_noclip()', description: `prevent clipping of sprites in status area` },
        { name: 'sp_nodraw()', description: `make sprite invisible` }
    ];

    const commands9 = [
        { name: 'compare_sprite_script()', description: `check for a specific script attached` },
        { name: 'compare_weapon()', description: `identify armed weapon by script name` },
        { name: 'external()', description: `call a procedure in another script` },
        { name: 'is_script_attached()', description: `detect a script attached to a sprite` },
        { name: 'kill_this_task()', description: `remove current script from memory` },
        { name: 'run_script_by_number()', description: `run script detected by is_script_attached()` },
        { name: 'script_attach()', description: `attach this script to another sprite` },
        { name: 'scripts_used()', description: `count scripts in memory` },
        { name: 'set_callback_random()', description: `repeatedly call another procedure in this script` },
        { name: 'sp_script()', description: `attach a named script to a sprite` },
        { name: 'spawn()', description: `invoke another script as a concurrent task` }
    ];

    const commands10 = [
        { name: 'kill_all_sounds()', description: `cancel all sound_set_survive() commands` },
        { name: 'load_sound()', description: `load sound into memory and give it a number` },
        { name: 'playmidi()', description: `play .mid file or CD track` },
        { name: 'playsound()', description: `play sound loaded by load_sound()` },
        { name: 'sound_set_kill()', description: `cancel a sound_set_survive() command` },
        { name: 'sound_set_survive()', description: `make a sound repeat and survive a screen change` },
        { name: 'sound_set_vol()', description: `reduce a sound's volume` },
        { name: 'sp_sound()', description: `attach a a sound with a sprite` },
        { name: 'stopcd()', description: `stop playing a CD track` },
        { name: 'stopmidi()', description: `stop a playmidi() playback` },
        { name: 'turn_midi_off()', description: `turn off midi attached to screen` },
        { name: 'turn_midi_on()', description: `undo turn_midi_off()` }
    ];

    const commands11 = [
        { name: 'busy()', description: `detect whether sprite is "talking"` },
        { name: 'compare_sprite_script()', description: `check for a specific script attached` },
        { name: 'create_sprite()', description: `dynamically create a sprite` },
        { name: 'disable_all_sprites()', description: `mass sp_disable() command` },
        { name: 'enable_all_sprites()', description: `undo disable_all_sprites()` },
        { name: 'get_rand_sprite_with_this_brain()', description: `pick sprite at random` },
        { name: 'get_sprite_with_this_brain()', description: `detect specific type of sprite` },
        { name: 'is_script_attached()', description: `detect a script attached to a sprite` },
        { name: 'hurt()', description: `hurt a sprite outside of combat situations` },
        { name: 'script_attach()', description: `attach this script to another sprite` },
        { name: 'say()', description: `associate text with a sprite` },
        { name: 'say_stop()', description: `associate text with a sprite, pause script` },
        { name: 'say_stop_npc()', description: `non-interfering text display` },
        { name: 'sp()', description: `get DinkEdit sprite # for dink engine sprite #` },
        { name: 'sp_editor_num()', description: `get dink engine sprite # for DinkEdit sprite #` }
    ];

    const commands12 = [
        { name: 'sp_base_death()', description: `DinkEdit: Shift 6` },
        { name: 'sp_active()', description: `DinkEdit: none [implied by creation of sprite]` },
        { name: 'sp_base_attack()', description: `DinkEdit: Alt 2` },
        { name: 'sp_base_idle()', description: `DinkEdit: unshifted 7` },
        { name: 'sp_base_walk()', description: `DinkEdit: unshifted 6` },
        { name: 'sp_brain()', description: `DinkEdit: unshifted 3` },
        { name: 'sp_defense()', description: `DinkEdit: Alt 3` },
        { name: 'sp_dir()', description: `DinkEdit: none` },
        { name: 'sp_disabled()', description: `DinkEdit: none [implied by creation of sprite]` },
        { name: 'sp_distance()', description: `DinkEdit: none` },
        { name: 'sp_exp()', description: `DinkEdit: none` },
        { name: 'sp_flying()', description: `DinkEdit: none` },
        { name: 'sp_follow()', description: `DinkEdit: none` },
        { name: 'sp_frame()', description: `DinkEdit: none` },
        { name: 'sp_frame_delay()', description: `DinkEdit: none` },
        { name: 'sp_gold()', description: `DinkEdit: none` },
        { name: 'sp_hitpoints()', description: `DinkEdit: Shift 8` },
        { name: 'sp_hard()', description: `DinkEdit: unshifted 9` },
        { name: 'sp_kill()', description: `DinkEdit: none` },
        { name: 'sp_move_nohard()', description: `DinkEdit: none` },
        { name: 'sp_move_nohard()', description: `DinkEdit: none` },
        { name: 'sp_mx()', description: `DinkEdit: none` },
        { name: 'sp_my()', description: `DinkEdit: none` },
        { name: 'sp_noclip()', description: `DinkEdit: none [related to Z or X plus arrows]` },
        { name: 'sp_nodraw()', description: `DinkEdit: none [similar to unshifted 2]` },
        { name: 'sp_nohit()', description: `DinkEdit: Shift 9` },
        { name: 'sp_pframe()', description: `DinkEdit: Tab then E, etc. [sprite edit mode]` },
        { name: 'sp_pseq()', description: `DinkEdit: " " " " " " " ` },
        { name: 'sp_que()', description: `DinkEdit: unshifted 8` },
        { name: 'sp_range()', description: `DinkEdit: none` },
        { name: 'sp_script()', description: `DinkEdit: Shift 5` },
        { name: 'sp_seq()', description: `DinkEdit: Shift 4` },
        { name: 'sp_size()', description: `DinkEdit: unshifted 1` },
        { name: 'sp_sound()', description: `DinkEdit: Shift 7` },
        { name: 'sp_speed()', description: `DinkEdit: unshifted 4` },
        { name: 'sp_strength()', description: `DinkEdit: none` },
        { name: 'sp_timing()', description: `DinkEdit: unshifted 5` },
        { name: 'sp_touch_damage()', description: `DinkEdit: Alt 1` },
        { name: 'sp_x()', description: `Dinkedit: set by sprite's placement` },
        { name: 'sp_y()', description: `Dinkedit: set by sprite's placement` }
    ];

    const commands13 = [
        { name: 'busy()', description: `detect whether a sprite is "talking"` },
        { name: 'initfont()', description: `set font for text` },
        { name: 'say()', description: `associate text with a sprite` },
        { name: 'say_stop()', description: `associate text with a sprite, pause script` },
        { name: 'say_stop_npc()', description: `non-interfering text display` },
        { name: 'say_stop_xy()', description: `display text anywhere on screen, pause script` },
        { name: 'say_xy()', description: `display text anywhere on screen` },
        { name: 'sp_brain()', description: `text sprites are brain 8` },
        { name: 'sp_kill()', description: `control how long a text sprite is displayed` }
    ];

    const commands14 = [
        { name: 'arm_magic()', description: `arm the spell in pack slot &cur_magic` },
        { name: 'arm_weapon()', description: `arm the weapon in pack slot &cur_weapon` },
        { name: 'compare_sprite_script()', description: `check for a specific script attached` },
        { name: 'get_version()', description: `get dink engine version` },
        { name: 'load_game()', description: `restore a saved game` },
        { name: 'game_exist()', description: `check to see whether save file SAVE#.DAT exists` },
        { name: 'get_burn()', description: `unknown` },
        { name: 'load_sound()', description: `load sound into memory and give it a number` },
        { name: 'set_button()', description: `change definition of game keys` },
        { name: 'sp_base_death()', description: `base value for death graphic` },
        { name: 'sp_base_idle()', description: `base value for standing-still seq's` },
        { name: 'sp_follow()', description: `make sprite follow another at a short distance` },
        { name: 'sp_gold()', description: `set a sprite's undocumented "gold" attribute` },
        { name: 'sp_nohit()', description: `test/apply DinkEdit "nohit" [Shift 9] property` },
        { name: 'sp_notouch()', description: `unknown` },
        { name: 'sp_picfreeze()', description: `unknown` }
    ];

    const commands15 = [
        { name: 'int', description: `define local variable` },
        { name: 'make_global_int()', description: `define global variable` }  
    ];

    const commands16 = [
        { name: 'activate_bow()', description: `start timed counter for firing bow` },
        { name: 'add_item()', description: `add a melee weapon to Dink's inventory` },
        { name: 'add_magic()', description: `add a magic attack to Dink's inventory` },
        { name: 'arm_magic()', description: `arm the spell in pack slot &cur_magic` },
        { name: 'arm_weapon()', description: `arm the weapon in pack slot &cur_weapon` },
        { name: 'compare_weapon()', description: `identify armed weapon by script name` },
        { name: 'get_last_bow_power()', description: `return activate_bow()'s counter` },
        { name: 'init()', description: `load sequences of Dink with or without his sword` },
        { name: 'kill_cur_item()', description: `delete currently armed melee weapon` },
        { name: 'kill_cur_magic()', description: `delete currently armed magic attack` },
        { name: 'kill_shadow()', description: `kill a sprite's "shadow" sprite` },
        { name: 'kill_this_item()', description: `delete a melee weapon` },
        { name: 'kill_this_magic()', description: `delete a magic attack` },
        { name: 'sp_attack_hit_sound()', description: `sound effect when weapon hits something` },
        { name: 'sp_attack_hit_sound_speed()', description: `pertains to above` },
        { name: 'sp_brain()', description: `brains 11 & 17 are missile brains` },
        { name: 'sp_distance()', description: `sets the range of melee weapons` },
        { name: 'sp_nocontrol()', description: `freeze sprite until an animation is played` }
    ];

    const commands17 = [
        { name: 'reset_timer()', description: `set game timer shown in save/load menus` },
        { name: 'debug()', description: `write text to debug.txt in debug mode` },
        { name: 'inside_box()', description: `detect sprite's general location` },
        { name: 'kill_game()', description: `exit to Windows` },
        { name: 'random()', description: `generate pseudo-random number` }
    ];
    
    const listOfCommands = [
        commands0,      // animation
        commands1,      // cut scene
        commands2,      // dink specific
        commands3,      // editor data
        commands4,      // enemy sprite
        commands5,      // inventory
        commands6,      // mouse
        commands7,      // player input
        commands8,      // screen drawing
        commands9,      // script management
        commands10,     // soound
        commands11,     // sprite
        commands12,     // sprite attributes
        commands13,     // text sprite
        commands14,     // undocumented
        commands15,     // variable definition
        commands16,     // weapon
        commands17      // none defined
    ];
    
    listOfCommands.forEach((commands, counter) => {
        const tableId = `cmd-table-${counter}`;
        let table = document.getElementById(tableId);
        table.innerHTML += generateRows(commands);
    });
}


