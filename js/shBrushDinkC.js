/**
 * SyntaxHighlighter
 * http://alexgorbatchev.com/
 *
 * SyntaxHighlighter is donationware. If you are using it, please donate.
 * http://alexgorbatchev.com/wiki/SyntaxHighlighter:Donate
 *
 * @version
 * 2.0.320 (May 03 2009)
 * 
 * @copyright
 * Copyright (C) 2004-2009 Alex Gorbatchev.
 *
 * @license
 * This file is part of SyntaxHighlighter.
 * 
 * SyntaxHighlighter is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * SyntaxHighlighter is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with SyntaxHighlighter.  If not, see <http://www.gnu.org/copyleft/lesser.html>.
 */
SyntaxHighlighter.brushes.DinkC = function()
{
	var datatypes =	'int void';
	var functions = 'activate_bow add_exp add_item add_magic arm_magic arm_weapon busy callback_kill choice_start choice_end clear_editor_info compare_magic compare_sprite_script compare_weapon copy_bmp_to_screen count_item count_magic create_sprite debug dink_can_walk_off_screen disable_all_sprites draw_background draw_hard_map draw_hard_sprite draw_screen editor_frame editor_seq editor_type enable_all_sprites external fade_down fade_up fill_screen force_vision free_items free_magic freeze game_exist get_burn get_date_day get_date_month get_date_year get_item get_last_bow_power get_magic get_next_sprite_with_this_brain get_rand_sprite_with_this_brain get_sprite_with_this_brain get_time_game get_time_real get_truecolor get_version hurt init initfont inside_box is_script_attached kill_all_sounds kill_cur_item kill_cur_magic kill_game kill_shadow kill_this_item kill_this_magic kill_this_task load_game load_map load_palette load_screen load_tile load_sound loopmidi make_global_function make_global_int math_abs math_mod math_sqrt map_hard_tile map_tile move move_stop playavi playmidi playsound preload_seq push_active random reset_timer run_script_by_number save_game say say_stop say_stop_npc say_stop_xy say_xy screenlock script_attach scripts_used set_button set_callback_random set_dink_base_punch set_dink_speed set_font_color set_keep_mouse key_mode set_smooth_follow set_save_game_info show_bmp show_console show_inventory sound_set_kill sound_set_survive sound_set_vol sp sp_active sp_attack_hit_sound sp_attack_hit_sound_speed sp_attack_wait sp_base_attack sp_base_death sp_base_die sp_base_idle sp_base_walk sp_blood_seq sp_blood_num sp_brain sp_brain_parm sp_brain_parm2 sp_clip_bottom sp_clip_left sp_clip_right sp_clip_top sp_custom sp_defense sp_dir sp_disabled sp_distance sp_editor_num sp_exp sp_flying sp_follow sp_frame sp_frame_delay sp_freeze sp_gold sp_hard sp_hitpoints sp_kill sp_kill_wait sp_move_nohard sp_mx sp_my sp_move_x sp_move_y sp_noclip sp_nocontrol sp_nodraw sp_nohit sp_notouch sp_pframe sp_picfreeze sp_pseq sp_que sp_range sp_reverse sp_script sp_seq sp_size sp_sound sp_speed sp_strength sp_target sp_timing sp_touch_damage sp_x sp_y spawn stop_entire_game stop_wait_for_button stopcd stopmidi title_start title_end turn_midi_off turn_midi_on unfreeze var_used wait wait_for_button';
	var keywords =	'if goto return';

	this.regexList = [
		{ regex: SyntaxHighlighter.regexLib.singleLineCComments,	css: 'comments' },      // one line comments
		{ regex: SyntaxHighlighter.regexLib.doubleQuotedString,		css: 'string' },        // strings
		{ regex: /&amp;[^,)\s\t;]+/gm,									css: 'variable' },      // variables (like &temp)
		{ regex: /^\s*\S+\:\s*$/gm,									css: 'preprocessor' },  // goto lines
		{ regex: new RegExp(this.getKeywords(keywords), 'gm'),		css: 'keyword' },       // keywords
		{ regex: new RegExp(this.getKeywords(functions), 'gm'),		css: 'functions' },     // functions
		{ regex: new RegExp(this.getKeywords(datatypes), 'gm'),		css: 'color1' },        // data types
		];
};

SyntaxHighlighter.brushes.DinkC.prototype	= new SyntaxHighlighter.Highlighter();
SyntaxHighlighter.brushes.DinkC.aliases	= ['dinkc', 'DinkC'];

