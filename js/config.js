function loadGlobals() {
    const cfgScroll = localStorage.getItem('cfgScroll');
    const cfgAnchorColor = localStorage.getItem('cfgAnchorColor');
    
    const cfgScrollExists = !!cfgScroll;
    const cfgAnchorColorExists = !!cfgAnchorColor;

    if (!cfgScrollExists) {
        localStorage.setItem('cfgScroll', true);
    }

    if(!cfgAnchorColorExists) {
        localStorage.setItem('cfgAnchorColor', 'true');
    }
}

function cfgScroll() {
    let btn = document.getElementById('cfgScroll');
    let config = cfgTrueFalse('cfgScroll');
    
    config = !config;
    localStorage.setItem('cfgScroll', config);

    newConfig = config ? 'smooth' : 'auto';

    document.getElementsByTagName('html')[0].style.scrollBehavior = newConfig;
    btn.innerText = buttonText(config);
}

function cfgAnchorColor() {
    let btn = document.getElementById('cfgAnchorColor');

    let config = cfgTrueFalse('cfgAnchorColor');
    let newClass = config ? 'tag-nostyle' : 'tag';
    
    let currentClass = config ? 'tag' : 'tag-nostyle';
    config = !config;
    var anchors = document.querySelectorAll('.' + currentClass);
    
    const length = anchors.length;
    for (let i = 0; i < length; i++) {
        anchors[i].classList.remove(currentClass);
        anchors[i].classList.add(newClass);
    }
    
    localStorage.setItem('cfgAnchorColor', config);
    btn.innerText = buttonText(config);
}

function cfgTrueFalse(cfgString) {
    const cfg = localStorage.getItem(cfgString);
    return (cfg === 'true') ? true : false;
}

function buttonText(config) {
    return config ? 'On' : 'Off';
}

function gitaheadtest() {
    // delete this function
}